import os
from functools import reduce
import hashlib
from natsort import natsort_keygen
from collections import namedtuple
from pymongo import MongoClient
import random

# A namedtuple for convenient handling of bounding box information.
BBOX = namedtuple('BBOX', ['category', 'xmin', 'xmax', 'ymin', 'ymax',
                           'score'])


def folderexists(foldername, create=False):
    """
    Determines if a folder exists. If it does not exist, and create=True,
    then creates the folder and returns the foldername. If it exists, then
    returns the foldername. If create=False, and the folder does not exist,
    then raises an OSError.
    :param foldername: string. Full path to the folder.
    :param create: bool. Whether to create the folder if it does not exist.
    :return: foldername. OSError if it does not exist and create=False.
    """
    if not os.path.isdir(foldername):
        if not create:
            raise OSError('The folder {} could not be found.'.format(
                foldername))
        else:
            os.makedirs(foldername)

    return foldername


def list_to_str(listcontents):
    """
    Represents a list as a string. The list can contain any elements which
    can be converted to a string. If the list is [a,b,c,d] then the output
    is "a, b, c and d". If the argument listcontents is not a list, then
    raises a ValueError. If the list contains elements which cannot be conver-
    ted to a string, then raises a TypeError.
    :param listcontents: A Python list containing elements which are or can be
    converted to a string.
    :return: A string representation of the list.
    """
    if not isinstance(listcontents, list):
        raise ValueError('The argument must be a list.')

    try:
        output = list(map(str, listcontents))
    except TypeError:
        raise TypeError('The list must contain elements which are string or '
                        'can be converted to a string.')
    output = ', '.join(listcontents[:-1])
    output = '{} and {}'.format(output, listcontents[-1])
    return output


def connect2mongo(hostname='localhost', port=27017):
    """
    Creates a MocatngoClient to the running mongod
    :param hostname: Hostname (default='localhost')
    :param port: Port number (default=27017)
    :return: An instance of MongoClient
    """
    client = MongoClient(hostname, port)
    return client


def getdb(mongoclient, databasename):
    """
    Gets a mongodb database
    :param mongoclient: An instance of MongoClient.
    :param databasename: Name of the database.
    :return: MongoDB database.
    """
    if not isinstance(databasename, str):
        raise ValueError('databasename must be a string.')

    db = mongoclient[databasename]
    return db


def get_hash(dictionary):
    """
    Takes a dictionary as input and provides a unique hash value based on the
    values in the dictionary. All the values in the dictionary after
    converstion to string are concatenated and then the HEX hash is generated
    :param dictionary: A python dictionary
    :return: A HEX hash
    """
    if not isinstance(dictionary, dict):
        raise ValueError('The argument must be ap ython dictionary.')

    str_input = reduce(lambda x, y: str(x) + str(y), list(dictionary.values()))
    str_input = ''.join(random.sample(str_input, len(str_input)))
    hash_object = hashlib.shake_128(str_input.encode())
    output = hash_object.hexdigest(12)
    return output


def natural_sort(listcontents):
    """
    Performs natural sorting on a list of filenames.
    The sorting key is the base name of the file without the path.
    :param listcontents: A list containing full path to filenames
    :return: Natural sorted version of the listcontents.
    """
    if not isinstance(listcontents, list):
        raise ValueError('The argument must be a list of strings.')

    natsort_key = natsort_keygen(key=lambda x: os.path.splitext(
        os.path.basename(x))[0])

    listcontents.sort(key=natsort_key)
    return listcontents


def merge_dict_lists(list1, list2, keyname):
    """
    Given two lists of dicts, and a keyname, merge the two lists into one
    list of dicts
    :param list1: A list of dicts
    :param list2: A list of dicts
    :param keyname: Name of the key. The key must be in each dict of list1
    and list2
    :return: A new list of dicts.
    """
    merged = {}
    for item in list1 + list2:
        if item[keyname] in merged:
            merged[item[keyname]].update(item)
        else:
            merged[item[keyname]] = item
    return [val for (_, val) in merged.items()]
